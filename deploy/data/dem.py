import pickle

with open ('../data/mtcnn_99/list_feature_mtcnn_99.p', 'rb') as fp:
    list_feature = pickle.load(fp)
with open ('../data/mtcnn_99/name_label_mtcnn_99.p', 'rb') as fp:
    list_label = pickle.load(fp)
with open ('../data/mtcnn_99/list_len_mtcnn_99.p', 'rb') as fp:
    list_len_imgs = pickle.load(fp)


print("list_feature: %d" %len(list_feature))
print("list_label: %d" %len(list_label))
print("list_len_imgs: %d" %len(list_len_imgs))
print(list_len_imgs)
