import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))
from connection import mysqlconnection
import pymysql.cursors
from flask import Flask, request
from flask_restful import Resource, Api
import json
from flask_jsonpify import jsonify

sys.path.append(os.path.join(os.path.dirname(__file__),'..'))
import face_model_v2 as face_model
from datetime import datetime
import argparse
import cv2
import numpy as np
import pandas as pd
import urllib
import urllib.request
import pickle
import time


parser = argparse.ArgumentParser(description='face model test')
# general
parser.add_argument('--image-size', default='112,112', help='')
parser.add_argument('--model', default='../../models/r100-arcface-mtcnn_99/model,0', help='path to load model.')
parser.add_argument('--ga-model', default='', help='path to load model.')
parser.add_argument('--gpu', default=0, type=int, help='gpu id')
parser.add_argument('--det', default=0, type=int, help='mtcnn option, 1 means using R+O, 0 means detect from begining')
parser.add_argument('--flip', default=0, type=int, help='whether do lr flip aug')
parser.add_argument('--threshold', default=1.24, type=float, help='ver dist threshold')

args = parser.parse_args()
model = face_model.FaceModel(args)

with open ('../data/mtcnn_99/list_feature_mtcnn_99.p', 'rb') as fp:
    list_feature = pickle.load(fp)
with open ('../data/mtcnn_99/name_label_mtcnn_99.p', 'rb') as fp:
    list_label = pickle.load(fp)

app = Flask(__name__)
api = Api(app)
class Student(Resource):
	def get(self,name_video):
		VIDEO_INPUT = "../../../admin_arcface/public/assets/videos/%s" %name_video
		vs = cv2.VideoCapture(VIDEO_INPUT)
		vs.set(cv2.CAP_PROP_POS_FRAMES, 0)
		i = 0
		sv = []
		list_mssv = []
		while True:
			ret, frame = vs.read()
			if not ret:
				break
			if i % 15 == 0:
				try:
					imgs,bbox,landmark = model.get_input(frame)
					for img_unit,bbox_unit,landmark_unit in zip(imgs, bbox, landmark):
						f = model.get_feature(img_unit)
						bien = False
						list_i = []
						list_dist = []
						for i in range(len(list_feature)):
							dist = np.sum(np.square(list_feature[i]-f))
							if dist < 1.1:
								list_dist.append(dist)
								list_i.append(i)
								bien = True
								if len(list_dist) >= 10:
									break
						if bien==True:
							x = min(list_dist)
							y = list_dist.index(x)
							i = list_i[y]
							mssv = list_label[i]
						acc = (len(list_dist)/10)*100
						if acc >= 80:
							list_mssv.append(mssv)
							sv = list(set(list_mssv))
				except Exception as e:
					pass
			i+=1
			
		return jsonify(sv)

class Image(Resource):
	def get(self,name_image):
		sv = []
		list_mssv = []
		IMAGE_PATH = "../../../admin_arcface/public/assets/images_2/%s" %name_image
		image = cv2.imread(IMAGE_PATH)
		try:
			imgs,bbox,landmark = model.get_input(image)
			for img_unit,bbox_unit,landmark_unit in zip(imgs, bbox, landmark):
				f = model.get_feature(img_unit)
				bien = False
				list_i = []
				list_dist = []
				for i in range(len(list_feature)):
					dist = np.sum(np.square(list_feature[i] - f))
					if dist < 1.1:
						list_dist.append(dist)
						list_i.append(i)
						bien = True
						if len(list_dist) >= 10:
							break
				if bien == True:
					x = min(list_dist)
					y = list_dist.index(x)
					i = list_i[y]
					mssv = list_label[i]
				acc = (len(list_dist)/10)*100
				if acc >= 80:
					list_mssv.append(mssv)
		except Exception as e:
			pass
		sv = list(set(list_mssv))

		return jsonify(sv)
class Camera(Resource):
	def get(self, action):
		if action is not None:
			i=0
			list_mssv = []
			sv = []
			cap = cv2.VideoCapture('http://admin:123456@10.105.10.170:8080/video')
			while True:
				ret, frame = cap.read()
				if not ret:
					break
				if action == "end":
					break
				if i%15 == 0:
					try:
						imgs,bbox,landmark = model.get_input(frame)
						for img_unit,bbox_unit,landmark_unit in zip(imgs, bbox, landmark):
							f = model.get_feature(img_unit)
							bien = False
							list_i = []
							list_dist = []
							for i in range(len(list_feature)):
								dist = np.sum(np.square(list_feature[i]-f))
								if dist < 1.1:
									list_dist.append(dist)
									list_i.append(i)
									bien = True
									if len(list_dist) >= 10:
										break
							if bien == True:
								x = min(list_dist)
								y = list_dist.index(x)
								i = list_i[y]
								mssv = list_label[i]
							acc = (len(list_dist)/10)*100
							if acc >= 80:
								list_mssv.append(mssv)
								sv = list(set(list_mssv))
					except Exception as e:
						pass
				i+=1
			return jsonify(sv)

api.add_resource(Student, '/student/<name_video>') # Route_1
api.add_resource(Image, '/image/<name_image>') # Route_2
api.add_resource(Camera, '/camera/<action>') # Route_3

if __name__ == '__main__':
     app.run()