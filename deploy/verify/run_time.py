import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))
import face_model_v2 as face_model
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))
from connection import mysqlconnection
import pymysql.cursors
from datetime import datetime,date
import argparse
import cv2
import numpy as np
import pandas as pd
import urllib
import urllib.request
import pickle
import time
parser = argparse.ArgumentParser(description='face model test')
# general
parser.add_argument('--image-size', default='112,112', help='')
parser.add_argument('--model', default='../../models/r100-arcface-mtcnn_99/model,0', help='path to load model.')
parser.add_argument('--ga-model', default='', help='path to load model.')
parser.add_argument('--gpu', default=0, type=int, help='gpu id')
parser.add_argument('--det', default=0, type=int, help='mtcnn option, 1 means using R+O, 0 means detect from begining')
parser.add_argument('--flip', default=0, type=int, help='whether do lr flip aug')
parser.add_argument('--threshold', default=1.24, type=float, help='ver dist threshold')
args = parser.parse_args()
model = face_model.FaceModel(args)

def insertRecord(mssv,image,log):
    if mssv is not None:
        try :
            connection = mysqlconnection.getConnection()
            cursor = connection.cursor()
            sql =  "Insert into check_logs (MSSV, image, time) " \
            + " values (%s, %s, %s)"
            # Thực thi sql và truyền 3 tham số
            dt_string = log.strftime("%d/%m/%Y %H:%M:%S")
            print(str(dt_string) + " " + str(mssv))
            cursor.execute(sql,(mssv,image,log))
            connection.commit()
        finally:
            connection.close()

img_root_save = '../../../admin_arcface/public/assets/image_compare'
color=(255, 0, 0)
bcolor=(0,0,255)
thickness = 2
font = cv2.FONT_HERSHEY_SIMPLEX
fontScale = 1
def Save_image(image,name,bbox):
    if image is not None:
        try:
            # pic = np.transpose(image,(1,2,0))[:, :, ::-1]
            box=bbox.astype(np.int).flatten()
            cv2.rectangle(image, (box[0],box[1]), (box[2],box[3]), bcolor,2)
            cv2.putText(image, name, (box[0],box[3]), font, fontScale, color, thickness, cv2.LINE_AA)
            cv2.imwrite(os.path.join(img_root_save,str(name)),image)
            print("Saved.")
        except Exception as e:
            print('Error occurred : ' + str(e))
        

with open ('../data/mtcnn_99/list_feature_mtcnn_99.p', 'rb') as fp:
    list_feature = pickle.load(fp)
with open ('../data/mtcnn_99/name_label_mtcnn_99.p', 'rb') as fp:
    list_label = pickle.load(fp)

# color=(255, 0, 0)
# bcolor=(0,0,255)
# thickness = 1
# font =cv2.FONT_HERSHEY_SIMPLEX
# fontScale = 1
vs = cv2.VideoCapture('http://admin:123456@192.168.0.154:8080/video')
# vs = cv2.VideoCapture('http://bao:123456@192.168.43.161:8888/mjpeg')
#vs = cv2.VideoCapture('rtsp://admin:123456@192.168.11.112:8554/live')
ii = 0
print("Starting program.")
while True:
    ret, frame = vs.read()
    if not ret:
        break
    if ii % 180 == 0:
        try:
            j = 0
            imgs,bbox,landmark = model.get_input(frame)
            for img_unit,bbox_unit,landmark_unit in zip(imgs, bbox, landmark):
                f = model.get_feature(img_unit)
                bien = False
                list_dist = []
                list_i = []
                for i in range(len(list_feature)):
                    dist = np.sum(np.square(list_feature[i]-f))
                    if dist < 1.1:
                        list_dist.append(dist)
                        list_i.append(i)
                        bien = True
                if bien==True:
                    x = min(list_dist)
                    if x <= 1.0:
                        y = list_dist.index(x)
                        i = list_i[y]
                        name = list_label[i]
                        name_image = str(date.today()) + '_' + name + '_' + str(i) + '.JPG'
                else:
                    name = 'unknown_' + str(ii) + '_' + str(j)
                    name_image = str(date.today()) + '_' + name + '_' + str(j) + '.JPG'
                j+=1
                acc = (len(list_dist)/10)*100
                if acc >= 90:
                    Save_image(frame, name_image, bbox_unit)
                    now = datetime.now()
                    insertRecord(name,name_image,now)
                if acc == 0:
                    Save_image(frame, name_image, bbox_unit)
                    now = datetime.now()
                    insertRecord(name,name_image,now)
        except Exception as e:
            print('Error occurred : ' + str(e))
    ii+=1
    #cv2.imshow('video',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

print("[INFO] cleaning up...")
vs.release()
cv2.destroyAllWindows()
print("Successfully!")