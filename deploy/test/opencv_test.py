import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))
import opencv_model as face_model

from datetime import datetime
import argparse
import cv2
import numpy as np
import pandas as pd
import pickle
import time

parser = argparse.ArgumentParser(description='face model test')
# general
parser.add_argument('--image-size', default='112,112', help='')
parser.add_argument('--model', default='../../models/r100-arcface-128vt/model,0', help='path to load model.')
parser.add_argument('--haarcasecade', default='../haarcascade_frontalface_default.xml', help='path to load haarcasecade.')
parser.add_argument('--gpu', default=0, type=int, help='gpu id')
parser.add_argument('--det', default=0, type=int, help='mtcnn option, 1 means using R+O, 0 means detect from begining')
parser.add_argument('--threshold', default=1.24, type=float, help='ver dist threshold')

args = parser.parse_args()
model = face_model.FaceModel(args)

print("STARTS")
#cap = cv2.VideoCapture('rtsp://admin:Bao123456@192.168.1.225/1')
cap = cv2.VideoCapture(0)
# cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
# cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
while True:
	ret, frame = cap.read()
	try:
		start = time.time()
		img, bbox,landmark = model.get_input(frame)
		for img_unit,bbox_unit,landmark_unit in zip(img, bbox, landmark):
			# f = model.get_feature(img_unit)
			box = bbox_unit.astype(np.int).flatten()
			cv2.rectangle(frame, (box[0],box[1]), (box[2],box[3]), (0,0,255),2)
			landmark = landmark_unit.astype(np.int).flatten()
			for i in range(len(landmark)-1):
				if i%2 == 0:
					cv2.circle(frame,(landmark[i],landmark[i+1]),3,(255,255,0),-1)
		end = time.time()
		print(end - start)
	except Exception as e:
		frame = frame
	cv2.imshow('Video', frame)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

cap.release()
cv2.destroyAllWindows()
