import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))
import face_model_v2 as face_model

from datetime import datetime
import argparse
import cv2
import numpy as np
import pandas as pd
import urllib
import urllib.request
import pickle
import time

parser = argparse.ArgumentParser(description='face model test')
# general
parser.add_argument('--image-size', default='112,112', help='')
parser.add_argument('--model', default='', help='path to load model.')
parser.add_argument('--haarcasecade', default='../haarcascade_frontalface_default.xml', help='path to load haarcasecade.')
parser.add_argument('--ga-model', default='', help='path to load model.')
parser.add_argument('--gpu', default=0, type=int, help='gpu id')
parser.add_argument('--det', default=0, type=int, help='mtcnn option, 1 means using R+O, 0 means detect from begining')
parser.add_argument('--flip', default=0, type=int, help='whether do lr flip aug')
parser.add_argument('--threshold', default=1.24, type=float, help='ver dist threshold')

args = parser.parse_args()
model = face_model.FaceModel(args)

# img = cv2.imread('../image-test/abc.jpg')

# imgs,bbox,landmark = model.get_input(img)
# for img_unit,bbox_unit,landmark_unit in zip(imgs, bbox, landmark):
# 	box = bbox_unit.astype(np.int).flatten()
# 	cv2.rectangle(img, (box[0],box[1]), (box[2],box[3]), (0,0,255),2)
# 	landmark = landmark_unit.astype(np.int).flatten()
# 	for i in range(len(landmark)-1):
# 		if i % 2 == 0:
# 			cv2.circle(img,(landmark[i],landmark[i+1]),3,(255,255,0),-1)
# cv2.imshow('image', img)
# cv2.waitKey(0)

#cap = cv2.VideoCapture('http://bao:123456@192.168.0.18:8888/mjpeg')
cap = cv2.VideoCapture(0)
# while True:
# 	ret, frame = cap.read()
# 	try:
# 		bbox = model.get_input(frame)
# 		for bbox_unit in bbox:
# 			print(bbox_unit)
# 			box = bbox_unit.astype(np.int).flatten()
# 			cv2.rectangle(frame, (box[0],box[1]), (box[2],box[3]), (0,0,255),2)
# 	except Exception as e:
# 		frame = frame
# 	cv2.imshow('Video', frame)
# 	if cv2.waitKey(1) & 0xFF == ord('q'):
# 		break

while True:
	ret, frame = cap.read()
	try:
		start = time.time()
		img, bbox,landmark = model.get_input(frame)
		for img_unit,bbox_unit,landmark_unit in zip(img, bbox, landmark):
			box = bbox_unit.astype(np.int).flatten()
			cv2.rectangle(frame, (box[0],box[1]), (box[2],box[3]), (0,0,255),2)
			landmark = landmark_unit.astype(np.int).flatten()
			for i in range(len(landmark)-1):
				if i%2 == 0:
					cv2.circle(frame,(landmark[i],landmark[i+1]),3,(255,255,0),-1)
		end = time.time()
		print(end - start)
	except Exception as e:
		frame = frame
	cv2.imshow('Video', frame)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

cap.release()
cv2.destroyAllWindows()