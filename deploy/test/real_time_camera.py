import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))
import face_model_v2 as face_model
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))
from connection import mysqlconnection
import pymysql.cursors
from datetime import datetime
import argparse
import cv2
import numpy as np
import pandas as pd
import urllib
import urllib.request
import pickle
import time
parser = argparse.ArgumentParser(description='face model test')
# general
parser.add_argument('--image-size', default='112,112', help='')
parser.add_argument('--model', default='../../models/r100-arcface-mtcnn_99/model,0', help='path to load model.')
parser.add_argument('--ga-model', default='', help='path to load model.')
parser.add_argument('--gpu', default=0, type=int, help='gpu id')
parser.add_argument('--det', default=0, type=int, help='mtcnn option, 1 means using R+O, 0 means detect from begining')
parser.add_argument('--flip', default=0, type=int, help='whether do lr flip aug')
parser.add_argument('--threshold', default=1.24, type=float, help='ver dist threshold')
args = parser.parse_args()
model = face_model.FaceModel(args)

def insertRecord(mssv,image,log):
    if mssv is not None:
        for x in mssv:
            try :
                connection = mysqlconnection.getConnection()
                cursor = connection.cursor()
                sql =  "Insert into check_logs (MSSV, image, time) " \
                + " values (%s, %s)"
                # Thực thi sql và truyền 3 tham số
                dt_string = log.strftime("%d/%m/%Y %H:%M:%S")
                print(dt_string." ".mssv)
                cursor.execute(sql,(x,log))
                connection.commit()
            finally:
                connection.close()

def Save_image():

with open ('../data/mtcnn_99/list_feature_mtcnn_99.p', 'rb') as fp:
    list_feature = pickle.load(fp)
with open ('../data/mtcnn_99/name_label_mtcnn_99.p', 'rb') as fp:
    list_label = pickle.load(fp)

# color=(255, 0, 0)
# bcolor=(0,0,255)
# thickness = 1
# font =cv2.FONT_HERSHEY_SIMPLEX
# fontScale = 1

#VIDEO_INPUT = "../image-test/5s.mp4"
vs = cv2.VideoCapture(0)
#vs.set(cv2.CAP_PROP_POS_FRAMES, 0)
i = 0
mssv = []
unknown = []
list_image1 = []
list_image2 = []
print("Starting program.")
start = time.time()
while True:
    ret, frame = vs.read()
    if not ret:
        break
    if i % 5 == 0:
        try:
            j = 0
            imgs,bbox,landmark = model.get_input(frame)
            for img_unit,bbox_unit,landmark_unit in zip(imgs, bbox, landmark):
                f = model.get_feature(img_unit)
                bien = False
                list_dist = []
                list_i = []
                for i in range(len(list_feature)):
                    dist = np.sum(np.square(list_feature[i]-f))
                    if dist < 1.1:
                        list_dist.append(dist)
                        list_i.append(i)
                        bien = True
                if bien==True:
                    x = min(list_dist)
                    if x <= 1.0:
                        y = list_dist.index(x)
                        i = list_i[y]
                        name = list_label[i]
                        name_image = str(datetime.now()) + '_' + str(j)
                else:
                    name_image = str(datetime.now()) + '_' + str(j)
                    name = 'unknown_' + str(j)
                j+=1
                acc = (len(list_dist)/10)*100
                end = time.time()
                count_time = end - start
                if acc >= 70:
                    mssv.append(name)
                    list_image1.append(name_image)
                    if count_time >= 20:
                        #sv = list(set(mssv))
                        now = datetime.now()
                        insertRecord(mssv,list_image1,now)
                        mssv.clear()
                        list_image1.clear()
                        start = end
                if acc == 0:
                    unknown.append(name)
                    list_image2.append(name_image)
                    if count_time >= 20:
                        now = datetime.now()
                        insertRecord(unknown,list_image2,now)
                        mssv.clear()
                        list_image.clear()
                        start = end
                
        except Exception as e:
            print('Error occurred : ' + str(e))
    i+=1
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

print("[INFO] cleaning up...")
vs.release()
cv2.destroyAllWindows()
print("Successfully!")