import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))
import face_model as face_model

from datetime import datetime
import argparse
import cv2
import numpy as np
import pandas as pd
import pickle
import time

parser = argparse.ArgumentParser(description='face model test')
# general
parser.add_argument('--image-size', default='112,112', help='')
parser.add_argument('--model', default='../../models/r100-arcface-mtcnn_99/model,0', help='path to load model.')
parser.add_argument('--ga-model', default='', help='path to load model.')
parser.add_argument('--gpu', default=0, type=int, help='gpu id')
parser.add_argument('--det', default=0, type=int, help='mtcnn option, 1 means using R+O, 0 means detect from begining')
parser.add_argument('--threshold', default=1.24, type=float, help='ver dist threshold')

args = parser.parse_args()
model = face_model.FaceModel(args)

with open ('../data/mtcnn_99/list_feature_mtcnn_99.p', 'rb') as fp:
    list_feature = pickle.load(fp)
with open ('../data/mtcnn_99/name_label_mtcnn_99.p', 'rb') as fp:
    list_label = pickle.load(fp)
with open ('../data/mtcnn_99/list_len_mtcnn_99.p', 'rb') as fp:
    list_len_imgs = pickle.load(fp)

color=(255, 0, 0)
bcolor=(0,0,255)
thickness = 1
font =cv2.FONT_HERSHEY_SIMPLEX
fontScale = 1

print("STARTS")
IMAGES_INPUT = "../images/du_lieu_sinh_vien/2"
images_save = "../val/2sinhvien"

if not os.path.exists(images_save):
    os.mkdir(images_save)

folders = os.listdir(IMAGES_INPUT)

start = time.time()
for folder in folders:
    imgs = os.listdir(os.path.join(IMAGES_INPUT,folder))
    cnt = 0
    for img in imgs:
        img_root_path = os.path.join(IMAGES_INPUT,folder,img)
        pic = cv2.imread(img_root_path)
        try:
            img,bbox,landmark = model.get_input(pic)
            f = model.get_feature(img)
            bien = False
            list_dist = []
            list_i = []
            list_len = []
            for i in range(len(list_feature)):
                dist = np.sum(np.square(list_feature[i]-f))
                if dist <= 1.24:
                    list_dist.append(dist)
                    list_i.append(i)
                    list_len.append(i)
                    bien = True
            if bien == True:
                x = min(list_dist)
                if x <= 1.1:
                    y = list_dist.index(x)
                    i = list_i[y]
                    ii = list_len[y]
                    name = list_label[i]
                    number = list_len_imgs[ii]
            else:
                name = "unknow!"
                number = 1
            acc = (len(list_dist)/number)*100
            box=bbox.astype(np.int).flatten()
            cv2.rectangle(pic, (box[0],box[1]), (box[2],box[3]), bcolor,2)
            cv2.putText(pic, str(acc)+"%", (box[0],box[1]), font, fontScale, color, thickness, cv2.LINE_AA)
            cv2.putText(pic, name, (box[0],box[3]), font, fontScale, color, thickness, cv2.LINE_AA)
            if not os.path.exists(os.path.join(images_save,folder)):
                    os.mkdir(os.path.join(images_save,folder))
            cv2.imwrite(os.path.join(images_save,folder,folder + '_00' + str(cnt) + '.JPG'),pic)
            cnt+=1
        except Exception as e:
            print('Error occurred : ' + str(e))

end = time.time()

interval = end - start

print("Total time: %d" %int(interval))
print("SuccessFully!")
