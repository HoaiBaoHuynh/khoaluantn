import face_model
import argparse
import cv2
import sys
import numpy as np

parser = argparse.ArgumentParser(description='face model test')
# general
parser.add_argument('--image-size', default='112,112', help='')
parser.add_argument('--model', default='', help='path to load model.')
parser.add_argument('--ga-model', default='', help='path to load model.')
parser.add_argument('--gpu', default=0, type=int, help='gpu id')
parser.add_argument('--det', default=0, type=int, help='mtcnn option, 1 means using R+O, 0 means detect from begining')
parser.add_argument('--flip', default=0, type=int, help='whether do lr flip aug')
parser.add_argument('--threshold', default=1.24, type=float, help='ver dist threshold')
args = parser.parse_args()

class Args():
    def __init__(self):
        self.image_size = '112,112'
        self.gpu = 0
        self.model = '../models/arcface_r100_v1/model,0000'
        self.ga_model = '../models/genderage_v1/model,0000'
        self.threshold = 1.24
        self.flip = 0
        self.det = 0

args = Args()
model = face_model.FaceModel(args)
print('bat dau test')
imgs_dir = 'images'
faces_save_dst = 'val/images/'

folders = os.listdir(imgs_dir)
lb = 0
for folder in folders:
    imgs = os.listdir(os.path.join(imgs_dir,folder))
    for file in imgs:
        print(file)
    n = len(imgs)
    cnt = 1
    for img in imgs:
        start = time.time()
        img_root_path = os.path.join(imgs_dir,folder,img)
        try:
            pic = cv2.imread(img_root_path)
            pic,bbox,landmark = model.get_input(pic)
            bbox = bbox.astype(np.int).flatten()
            landmark = landmark.astype(np.int).flatten()
            #print(bbox)
            #print(landmark)
            info = '1\t' + './' + img_root_path + '\t' + str(lb) + '\t' + str(bbox[0]) + '\t' + str(bbox[1]) + '\t' + str(bbox[2]) + '\t' + str(bbox[3])
            for i in range(len(landmark)):
                info = info + '\t' + str(landmark[i])
            info = info + '\n'
            file = open('val/train.lst','a')
            file.write(info)
            #feature = model.get_feature(pic)
            #print(pic)
            if type(pic) == np.ndarray:
                if not os.path.exists(os.path.join(faces_save_dst,folder)):
                    os.mkdir(os.path.join(faces_save_dst,folder))
                if cnt == 10:
                    cv2.imwrite(os.path.join(faces_save_dst,folder,folder + '_00' + str(cnt) + '.JPG'),np.transpose(pic,(1,2,0))[:, :, ::-1])
                else:
                    cv2.imwrite(os.path.join(faces_save_dst,folder,folder + '_000' + str(cnt) + '.JPG'),np.transpose(pic,(1,2,0))[:, :, ::-1])
                cnt+=1
            end = time.time()
            
            interval = end - start
        except Exception as e:
            print('Error occurred : ' + str(e))
    lb+=1