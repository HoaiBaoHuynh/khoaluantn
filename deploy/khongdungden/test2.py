import face_model
import argparse
import cv2
import sys
import numpy as np
from numpy.linalg import norm

parser = argparse.ArgumentParser(description='face model test')
# general
parser.add_argument('--image-size', default='112,112', help='')
parser.add_argument('--model', default='', help='path to load model.')
parser.add_argument('--ga-model', default='', help='path to load model.')
parser.add_argument('--gpu', default=0, type=int, help='gpu id')
parser.add_argument('--det', default=0, type=int, help='mtcnn option, 1 means using R+O, 0 means detect from begining')
parser.add_argument('--flip', default=0, type=int, help='whether do lr flip aug')
parser.add_argument('--threshold', default=1.24, type=float, help='ver dist threshold')
args = parser.parse_args()

class Args():
    def __init__(self):
        self.image_size = '112,112'
        self.gpu = 0
        self.model = '../models/arcface_r100_512/model,0000'
        self.ga_model = '../models/genderage_v1/model,0000'
        self.threshold = 1.24
        self.flip = 0
        self.det = 0

args = Args()
model = face_model.FaceModel(args)
img1 = cv2.imread('./Bao/Bao_0010.JPG')
img2 = cv2.imread('./Bao/Bao_0008.JPG')
img1,bb,lm = model.get_input(img1)
img2,bb,lm = model.get_input(img2)
f12 = None
f1 = model.get_feature(img1)
f2 = model.get_feature(img2)
fx = (f1 + f2)
f12 = fx/2
print("f12: " ,f12[0:10])
#fn = np.sum(np.square(f1))
#print("f1: ",f1)
#print("fn: ",fn)
#gender, age = model.get_ga(img)
#fgender = 'Nam'
#if gender==0:
    #fgender = 'Nữ'
#print("Giới tính: ",fgender)
#print("Tuổi: ",age)
#sys.exit(0)
img3 = cv2.imread('./Bao/Bao_0004.JPG')
img3,bb2,lm2 = model.get_input(img3)
f3 = model.get_feature(img3)
#fn2 = np.sum(np.square(f2))
#print("fn2: ",fn2)
print("f3: ",f3[0:10])
dist = np.sum(np.square(f12-f3))
if dist < args.threshold:
    print("Cùng một người.")
else:
    print("2 người khác nhau.")
print("dist: ",dist)
sim = np.dot(f12, f3.T)
print("Sim: ",sim)
#diff = np.subtract(source_feature, target_feature)
#dist = np.sum(np.square(diff),1)