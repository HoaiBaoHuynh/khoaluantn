import face_model_v2 as face_model
import urllib
import urllib.request
import argparse
import cv2
import sys
import pickle
import numpy as np
import pandas as pd
import os
parser = argparse.ArgumentParser(description='face model test')
# general
parser.add_argument('--image-size', default='112,112', help='')
parser.add_argument('--model', default='../models/r100-arcface-emore/model,0', help='path to load model.')
parser.add_argument('--ga-model', default='', help='path to load model.')
parser.add_argument('--gpu', default=0, type=int, help='gpu id')
parser.add_argument('--det', default=0, type=int, help='mtcnn option, 1 means using R+O, 0 means detect from begining')
parser.add_argument('--flip', default=0, type=int, help='whether do lr flip aug')
parser.add_argument('--threshold', default=1.24, type=float, help='ver dist threshold')
#parser.add_argument('--videoPath', default='./testv.mp4')
#parser.add_argument('--binPath', default=0)
args = parser.parse_args()
model = face_model.FaceModel(args)
 # Get the persistent feature library and the corresponding label
threshold = float(args.threshold)
#print(threshold)
with open ('data/list_feature_emore.p', 'rb') as fp:
    list_feature = pickle.load(fp)
with open ('data/name_label_emore.p', 'rb') as fp:
    list_label = pickle.load(fp)
    
def url_to_image(url):
    resp = urllib.request.urlopen(url)
    image = np.asarray(bytearray(resp.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)
    return image
url = 'https://scontent.fsgn3-1.fna.fbcdn.net/v/t1.15752-9/100481508_1950251255107660_5071376587161600000_n.png?_nc_cat=107&_nc_sid=b96e70&_nc_ohc=Vnzk9EPTr1IAX9lvoY2&_nc_ht=scontent.fsgn3-1.fna&oh=4836cf5d6e32b8fc7a04fdd6423918e9&oe=5EEB7083'
pic = url_to_image(url) 
imgs,bbox,landmark = model.get_input(pic)
for img_unit,bbox_unit,landmark_unit in zip(imgs, bbox, landmark):
    if img_unit.shape:
        f = model.get_feature(img_unit)
        bien = False
        list_dist = []
        list_i = []
        for i in range(len(list_feature)):
            dist = np.sum(np.square(list_feature[i]-f))
            if dist < 1.1:
                list_dist.append(dist)
                list_i.append(i)
                bien = True
        if bien==True:
            x = min(list_dist)
            if x <= 1.0:
                y = list_dist.index(x)
                i = list_i[y]
                name = list_label[i]
        else:
            name = "Unknow!"
        bbox_unit=bbox_unit.astype(np.int).flatten()
        acc = (len(list_dist)/10)*100        
        cv2.rectangle(pic, (int(bbox_unit[0]), int(bbox_unit[1])),(int(bbox_unit[2]), int(bbox_unit[3])), (0,0,255), 2)
        cv2.putText(pic, name, (int(bbox_unit[0]), int(bbox_unit[3])), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2, cv2.LINE_AA)
        cv2.putText(pic, str(acc) + "%", (int(bbox_unit[0]), int(bbox_unit[1])), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2, cv2.LINE_AA)
        landmark_unit=landmark_unit.astype(np.int).flatten()
        for i in range(len(landmark_unit)-1):
            if i % 2 == 0:
                cv2.circle(pic,(landmark_unit[i],landmark_unit[i+1]),3,(255,255,0),-1)
print('final')
filename = './test.jpg'
print('writing', filename)
cv2.imwrite(filename, pic)