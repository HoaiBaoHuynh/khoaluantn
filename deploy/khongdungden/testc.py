import face_model_v2 as face_model
import argparse
import cv2
import sys
import numpy as np
import pandas as pd
import pickle
parser = argparse.ArgumentParser(description='face model test')
# general
parser.add_argument('--image-size', default='112,112', help='')
parser.add_argument('--model', default='../models/r100-arcface-69/model,4', help='path to load model.')
parser.add_argument('--ga-model', default='', help='path to load model.')
parser.add_argument('--gpu', default=0, type=int, help='gpu id')
parser.add_argument('--det', default=0, type=int, help='mtcnn option, 1 means using R+O, 0 means detect from begining')
parser.add_argument('--flip', default=0, type=int, help='whether do lr flip aug')
parser.add_argument('--threshold', default=1.24, type=float, help='ver dist threshold')
args = parser.parse_args()
model = face_model.FaceModel(args)

with open ('data/list_feature_69_04.p', 'rb') as fp:
    list_feature = pickle.load(fp)
with open ('data/name_label_69_04.p', 'rb') as fp:
    list_label = pickle.load(fp)

color=(255, 0, 0)
bcolor=(0,0,255)
thickness = 1
font =cv2.FONT_HERSHEY_SIMPLEX
fontScale = 1

print('*'*8, 'into video')
cap = cv2.VideoCapture(0)
print('get video')
i = 0
while True:
	ret, frame = cap.read()
	if i%1 == 0:
		try:
			imgs,bbox,landmark = model.get_input(frame)
			for img_unit,bbox_unit,landmark_unit in zip(imgs, bbox, landmark):
				f = model.get_feature(img_unit)

				bien = False
				list_dist = []
				list_i = []

				for i in range(len(list_feature)):
					dist = np.sum(np.square(list_feature[i]-f))
					if dist < 1.1:
						list_dist.append(dist)
						list_i.append(i)
						bien = True
				if bien==True:
					x = min(list_dist)
					if x <= 1.0:
						y = list_dist.index(x)
						i = list_i[y]
						name = list_label[i]
				else:
					name = "Unknow!"
				acc = (len(list_dist)/10)*100
				box=bbox_unit.astype(np.int).flatten()
				cv2.rectangle(frame, (box[0],box[1]), (box[2],box[3]), bcolor,2)
				cv2.putText(frame, str(acc)+"%", (box[0],box[1]), font, fontScale, color, thickness, cv2.LINE_AA)
				cv2.putText(frame, name, (box[0],box[3]), font, fontScale, color, thickness, cv2.LINE_AA)
				# landmark = landmark_unit.astype(np.int).flatten()
				# for i in range(len(landmark)-1):
				# 	if i % 2 == 0:
				# 		cv2.circle(frame,(landmark[i],landmark[i+1]),3,(255,255,0),-1)
		except Exception as e:
			frame = frame
	cv2.imshow('Video', frame)
	i+=1
	if cv2.waitKey(1) & 0xff == ord('q'):
		break
cap.release()
cv2.destroyAllWindows()